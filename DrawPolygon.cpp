#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	float PI = 3.14;
	int x[3], y[3];
	float phi = PI / 2;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 3;

	}
	for (int i = 0; i < 3; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3], ren);

	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	float PI = 3.14;
	int x[4], y[4];
	float phi = PI / 4;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += PI / 2;

	}
	Bresenham_Line(x[0], y[0], x[1], y[1], ren);
	Bresenham_Line(x[1], y[1], x[2], y[2], ren);
	Bresenham_Line(x[2], y[2], x[3], y[3], ren);
	Bresenham_Line(x[3], y[3], x[0], y[0], ren);

}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	float PI = 3.14;
	int x[5], y[5];
	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;

	}

	Bresenham_Line(x[0], y[0], x[1], y[1], ren);
	Bresenham_Line(x[1], y[1], x[2], y[2], ren);
	Bresenham_Line(x[2], y[2], x[3], y[3], ren);
	Bresenham_Line(x[3], y[3], x[4], y[4], ren);
	Bresenham_Line(x[4], y[4], x[0], y[0], ren);

}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	float PI = 3.14;
	int x[6], y[6];
	float phi = 0;
	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += PI / 3;

	}
	Bresenham_Line(x[0], y[0], x[1], y[1], ren);
	Bresenham_Line(x[1], y[1], x[2], y[2], ren);
	Bresenham_Line(x[2], y[2], x[3], y[3], ren);
	Bresenham_Line(x[3], y[3], x[4], y[4], ren);
	Bresenham_Line(x[4], y[4], x[5], y[5], ren);
	Bresenham_Line(x[5], y[5], x[0], y[0], ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	float PI = 3.14;
	int x[5], y[5];
	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;

	}
	Bresenham_Line(x[0], y[0], x[2], y[2], ren);
	Bresenham_Line(x[0], y[0], x[3], y[3], ren);
	Bresenham_Line(x[1], y[1], x[3], y[3], ren);
	Bresenham_Line(x[1], y[1], x[4], y[4], ren);
	Bresenham_Line(x[4], y[4], x[2], y[2], ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	float PI = 3.14;
	int x[5], y[5];
	int xn[5], yn[5];
	float phi = PI / 2;
	float phinho = R*sin(PI / 10) / sin(7 * PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xn[i] = xc + int(phinho * cos(phi + PI / 5) + 0.5);
		yn[i] = yc - int(phinho * sin(phi + PI / 5) + 0.5);
		phi += 2 * PI / 5;
	}
	for (int i = 0; i < 4; i++)
	{
		Bresenham_Line(x[i], y[i], xn[i], yn[i], ren);
		Bresenham_Line(xn[i], yn[i], x[i + 1], y[i + 1], ren);
	}
	Bresenham_Line(x[4], y[4], xn[4], yn[4], ren);
	Bresenham_Line(xn[4], yn[4], x[0], y[0], ren);
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	float PI = 3.14;
	int x[8], y[8];
	int xn[8], yn[8];
	float phi = 0;
	float phinho = R*sin(PI / 10) / sin(7 * PI / 10);
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xn[i] = xc + int(phinho * cos(phi + PI / 8) + 0.5);
		yn[i] = yc - int(phinho * sin(phi + PI / 8) + 0.5);
		phi += PI / 4;
		SDL_RenderDrawPoint(ren, x[i], y[i]);
		SDL_RenderDrawPoint(ren, xn[i], yn[i]);
	}
	for (int i = 0; i < 7; i++)
	{
		Bresenham_Line(x[i], y[i], xn[i], yn[i], ren);
		Bresenham_Line(xn[i], yn[i], x[i + 1], y[i + 1], ren);
	}
	Bresenham_Line(x[7], y[7], xn[7], yn[7], ren);
	Bresenham_Line(xn[7], yn[7], x[0], y[0], ren);



}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	float PI = 3.14;
	int x[5], y[5];
	int xn[5], yn[5];
	float phi = startAngle;
	float rnho = R*sin(PI / 10) / sin(7 * PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xn[i] = xc + int(rnho * cos(phi + PI / 5) + 0.5);
		yn[i] = yc - int(rnho * sin(phi + PI / 5) + 0.5);
		phi += 2 * PI / 5;
	}
	for (int i = 0; i < 4; i++)
	{
		Bresenham_Line(x[i], y[i], xn[i], yn[i], ren);
		Bresenham_Line(xn[i], yn[i], x[i + 1], y[i + 1], ren);
	}
	Bresenham_Line(x[4], y[4], xn[4], yn[4], ren);
	Bresenham_Line(xn[4], yn[4], x[0], y[0], ren);
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float PI = 3.14;
	float gbd = PI / 2;
	while (r > 1)
	{
		DrawStarAngle(xc, yc, int(r+0.5), gbd, ren);
		gbd =gbd +  PI;
		r = r*sin(PI / 10) / sin(7 * PI / 10);
	}
}
