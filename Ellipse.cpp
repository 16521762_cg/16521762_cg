#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	
	int new_x;
	int new_y;
	//Point 1
	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	//Point 2
	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	//Point 3
	new_x = xc + x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	//Point 4
	new_x = xc - x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x = 0;
	int y = b;
	int p = -(2 * a*a)*b + a*a + 2 * b*b;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b*b) <= a*a*a*a)
	{
		if (p <= 0)
		{
			p += 4 * b*b*x + 6 * b*b;
		}
		else
		{
			p += 4 * b*b*x - 4 * a*a*y + (4 * a*a + 6 * b*b);
			y = y - 1;
		}
		x = x + 1;
		Draw4Points(xc, yc, x, y, ren);
	}
    // Area 2
	x = a;
	y = 0;
	p = 2 * a*a - (2 * b*b)*a + b*b;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b*b) >= a*a*a*a)
	{
		if (p <= 0)
		{
			p += 4 * a*a*y + 6 * a*a;
		}
		else
		{
			p += -4 * x*b*b + 4 * a*a*y + 6 * a*a + 4 * b*b;
			x = x - 1;
		}
		y = y + 1;
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
	int x, y, fx, fy, a2, b2, p;
	x = 0;
	y = b;
	fx = 0;
	fy = 2 * a*a * y;
	Draw4Points(xc, yc, x, y, ren);
	p = b*b - a*a*b + (a*a) *0.25;
	while (fx<fy)
	{
		x = x + 1;
		fx += 2 * b*b;
		if (p<0)
		{
			p += b*b*(2 * x + 3);
		}
		else
		{
			y = y - 1;
			p += b*b*(2 * x + 3) + a*a*(2 - 2 * y);
			fy -= 2 * a*a;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	p = (b*b*(x + 0.5)*(x + 0.5) + a*a*(y - 1)*(y - 1) - a*a*b*b);
	while (y>0)
	{
		y = y - 1;
		fy -= 2 * a*a;
		if (p >= 0)
		{
			p += a*a*(3 - 2 * y);
		}
		else
		{
			x=x+1;
			p += b*b*(2 * x + 2) + a*a*(3 - 2 * y);
			fx += 2 * b*b;
		}
		Draw4Points(xc, yc, x, y, ren);
	}

}