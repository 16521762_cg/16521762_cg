#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"
#include "Circle.h"
#include "Ellipse.h"
using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE
	SDL_Color fillcolor;
	fillcolor.a = 255;
	fillcolor.b = 0;
	fillcolor.g = 255;
	fillcolor.r = 255;
	SDL_Color fillcolor1;
	fillcolor1.a = 255;
	fillcolor1.b = 0;
	fillcolor1.g = 0;
	fillcolor1.r = 255;
	SDL_Color fillcolor2;
	fillcolor2.a = 255;
	fillcolor2.b = 255;
	fillcolor2.g = 0;
	fillcolor2.r = 255;
	Vector2D V1(100, 100);
	Vector2D V2(250, 150);
	Vector2D V3(100, 200);
	Vector2D V4(150, 200);
	SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
	//TriangleFill(V1, V2, V3, ren, fillcolor2);
	//RectangleFill(V1, V2, ren, fillcolor);
	//CircleFill(200, 200, 100, ren, fillcolor);
	//FillIntersectionTwoCircles(200, 200, 100, 150, 150, 100, ren, fillcolor2);
	//CircleFill(100, 100, 100, ren, fillcolor1);
	//BresenhamDrawEllipse(150, 250, 150, 100, ren);
	//FillIntersectionRectangleCircle(V1, V2, 100, 100, 50, ren, fillcolor2);
	//CircleFill(150, 150, 50, ren, fillcolor);
	//FillIntersectionTwoCircles(100, 100, 100, 150, 150, 50, ren, fillcolor2);
	//FillIntersectionEllipseCircle(150, 250, 150, 100, 100, 100, 100, ren, fillcolor);

	SDL_SetRenderDrawColor(ren, 255, 0, 255, 255);
	Uint32 pixel_format = SDL_GetWindowPixelFormat(win); 
	BresenhamDrawEllipse(200, 250, 5, 7, ren);
	Vector2D VV(200, 250);
	BoundaryFill4(win,VV, pixel_format, ren, fillcolor, fillcolor2);
	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event

	bool running = true;
	Vector2D V;
	while (running)
	{
		
		SDL_RenderPresent(ren);
		int x1 = V1.x, x2 = V2.x, x3 = V3.x, x4=V4.x;
		int y1 = V1.y, y2 = V2.y, y3 = V3.y, y4=V4.y;
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			//If the user has Xed out the window
			/*if (event.type == SDL_MOUSEBUTTONDOWN) {
				int x, y;
				SDL_GetMouseState(&x, &y);
				if ((x - x1)*(x - x1) + (y - y1)*(y - y1) <= 10 * 10) V = V1; 
				if ((x - x2)*(x - x2) + (y - y2)*(y - y2) <= 10 * 10) V = V2; 
				if ((x - x3)*(x - x3) + (y - y3)*(y - y3) <= 10 * 10) V = V3; 
				if ((x - x4)*(x - x4) + (y - y4)*(y - y4) <= 10 * 10) V = V4;
			}
			if (event.type == SDL_MOUSEMOTION) {
				int x, y;
				SDL_GetMouseState(&x, &y);
				if (V.x == x1&&V.y == y1) V1.x = x, V1.y = y, V.x = x, V.y = y; 
				if (V.x == x2&&V.y == y2) V2.x = x, V2.y = y, V.x = x, V.y = y; 
				if (V.x == x3&&V.y == y3) V3.x = x, V3.y = y, V.x = x, V.y = y; 
				if (V.x == x4&&V.y == y4) V4.x = x, V4.y = y, V.x = x, V.y = y;
			}
			SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
			SDL_RenderClear(ren);
			SDL_SetRenderDrawColor(ren, 255, 255, 0, 255);
			//DrawCurve2(ren, V1, V2, V3);
			//DrawCurve3(ren, V1, V2, V3, V4);
			SDL_RenderPresent(ren);
			if (event.type == SDL_MOUSEBUTTONUP) {
				V = NULL;
			}*/

			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
