#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    //draw 2 points
	SDL_RenderDrawPoint(ren, xc, yc);
	SDL_RenderDrawPoint(ren, x, y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0;
	int y = 0;
	Draw2Points(xc, yc, x, y, ren);
	double d1;
	d1 = (2 * A) - 1;
	Draw2Points(xc, yc, x, y, ren);
	while (y <= (2 * A*1.0))
	{
		if (d1<0)
		{
			d1 += 4 * A - 3 - 2 * y;
			x++;
			y++;
		}
		else
		{
			d1 -= 3 + 2 * y;
			y++;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
	d1 = (4.0*A*(x + 1) - (y + 0.5)*(y + 0.5));
	while (y < 600)
	{
		if (d1<0)
		{
			d1 += 4 * A;
			x++;
		}
		else
		{
			d1 += 4.0*A - 2 - 2.0*y;
			x++;
			y++;
		}
		Draw2Points(xc, yc, x, y, ren);

	}
	
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0;
	int y = 0;
	int g = y*y - 2 * x*y + x*x - 14 * A*x - 14 * A*y;
	int p = 4 * g + 12 * x - 12 * y - 28*A + 9;
	Draw2Points(xc, yc, x, y, ren);
	while (x <=A )
	{
		x++;
		if (p>0)
		{ 
			g += -2 * y + 2 * x + 1 - 14 * A;
			p += -8 * y + 8 * x - 56 * A + 16;
		}
		else
		{
			g += -4 * y + 4 * x + 4;
			p += -16 * y + 16 * x + 40;
			y--;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
	p = g - 14 * A;
	Draw2Points(xc, yc, x, y, ren);
	while (x>A)
	{
		
		if (p>0)
		{
			x++;
			g += -2 * y + 2 * x + 1 - 14 * A;
			p += -2 * y + 2 * x - 14 * A + 1;
		}
		else
		{
			g += 2 * y - 2 * x + 1 - 14*A;
			p += -2 * x + 2 * y - 14 * A +1;
			y++;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
	/*while (p > 0)
	{
		x++;
		g += -2 * y + 2 * x + 1 - 14 * A;
		p += -8 * y + 8 * x - 56 * A + 16;
		Draw2Points(xc, yc, x, y, ren);
	}
	while (p <= 0)
	{
		x++;
		g += -4 * y + 4 * x + 4;
		p += -16 * y + 16 * x + 40;
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}*/
}